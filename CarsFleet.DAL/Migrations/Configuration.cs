namespace CarsFleet.DAL.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CarsFleet.DAL.Database.CarsContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CarsFleet.DAL.Database.CarsContext context)
        {
            context.Users.AddOrUpdate(
                p => p.Id,
                new Models.User { Id = 1, FirstName = "Jan", LastName = "Kowalski" },
                new Models.User { Id = 2, FirstName = "Adam", LastName = "Nowak" },
                new Models.User { Id = 3, FirstName = "Ma�gorzata", LastName = "W�jcik" });
        }
    }
}
