﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Autofac;
using CarsFleet.DAL.Interfaces;
using CarsFleet.DAL.Services;
using System.Reflection;
using CarsFleet.DAL.Database;

namespace CarsFleet.DAL
{
    public class IoCModule : Autofac.Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //builder.RegisterType<CarsService>().As<ICarsService>();
            var currentAssembly = Assembly.GetExecutingAssembly();
            builder.RegisterAssemblyTypes(currentAssembly)
                .Where(type => type.Namespace == "CarsFleet.DAL.Services")
                .AsImplementedInterfaces();
            builder.RegisterType<CarsContext>().AsSelf().InstancePerRequest();
        }
    }
}
