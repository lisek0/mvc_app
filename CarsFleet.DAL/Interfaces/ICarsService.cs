﻿using CarsFleet.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsFleet.DAL.Interfaces
{
    public interface ICarsService
    {
        IEnumerable<Car> GetAllCars();
        Car GetById(int id);
        bool Create(Car car);
        bool SetCarUser(int carId, int userId);
    }
}
