﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsFleet.DAL.Enums
{
    public enum ECarType
    {
        Car,
        Truck
    }
}
