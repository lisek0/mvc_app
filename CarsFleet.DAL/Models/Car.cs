﻿using CarsFleet.DAL.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsFleet.DAL.Models
{
    public class Car
    {
        public int Id { get; set; }
        public decimal? Capacity { get; set; }
        public string Mark { get; set; }
        public ECarType Type { get; set; } 
        public virtual User User { get; set; }
    }

}
