﻿using CarsFleet.DAL.Database;
using CarsFleet.DAL.Interfaces;
using CarsFleet.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CarsFleet.DAL.Services
{
    public class UserService : IUserService
    {
        private readonly CarsContext _carsContext;
        public UserService(Database.CarsContext carsContext)
        {
            _carsContext = carsContext;
        }
        public IEnumerable<User> GetAllUsers()
        {
            return _carsContext.Users;
        }
    }
}
