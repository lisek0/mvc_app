﻿using CarsFleet.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CarsFleet.DAL.Models;
using CarsFleet.DAL.Database;

namespace CarsFleet.DAL.Services
{
    public class CarsService : ICarsService
    {
        private readonly CarsContext _dbContext;
        public CarsService(CarsContext dbContext)
        {
            _dbContext = dbContext;
        }

        public bool Create(Car car)
        {
            try
            {
                _dbContext.Cars.Add(car);
                _dbContext.SaveChanges();
                return true;
            } catch(Exception ex)
            {
                return false;
            }
        }

        public IEnumerable<Car> GetAllCars()
        {
            return _dbContext.Cars;//.Include("User");
        }

        public Car GetById(int id)
        {
            return _dbContext.Cars.FirstOrDefault(item => item.Id == id); //.Include("User")
        }

        public bool SetCarUser(int carId, int userId)
        {
            var car = _dbContext.Cars.FirstOrDefault(c => c.Id == carId);
            var user = _dbContext.Users.FirstOrDefault(u => u.Id == userId);
            if(car != null && user != null)
            {
                car.User = user;
                _dbContext.SaveChanges();
                return true;
            } else
            {
                return false;
            }
        }
    }
}
