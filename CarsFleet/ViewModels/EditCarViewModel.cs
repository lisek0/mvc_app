﻿using CarsFleet.DAL.Enums;
using CarsFleet.DAL.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarsFleet.ViewModels
{
    public class EditCarViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Marka")]
        public string Mark { get; set; }
        [Display(Name = "Typ pojazdu")]
        public ECarType Type { get; set; }
        [Required]
        [Display(Name = "Użytkownik")]
        public int UserId { get; set; }

        public EditCarViewModel()
        {

        }
        public EditCarViewModel(Car car)
        {
            if(car != null)
            {
                Id = car.Id;
                Mark = car.Mark;
                Type = car.Type;
                if(car.User != null)
                {
                    UserId = car.User.Id;
                }

            }
        }
    }
}