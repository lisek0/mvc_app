﻿using CarsFleet.DAL.Enums;
using CarsFleet.DAL.Models;
using ExpressiveAnnotations.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CarsFleet.ViewModels
{
    public class CarsViewModel
    {
        private Car _car;
        public CarsViewModel()
        {

        }
        public CarsViewModel(Car car)
        {
            if (car != null)
            {
                Id = car.Id;
                Type = car.Type;
                Mark = car.Mark;
                Capacity = car.Capacity;
                if (car.User != null)
                {
                    UserName = $"{car.User.FirstName} {car.User.LastName}";
                }

            }
        }

        public Car GetModel()
        {
            return new Car
            {
                Id = Id,
                Capacity = Capacity,
                Mark = Mark,
                Type = Type
     
            };
        }

        public int Id { get; set; }
        [Required]
        [Display(Name = "Typ pojazdu")]
        public ECarType Type { get; set; }
        [Required]
        [Display(Name = "Marka")]
        public string Mark { get; set; }
        [RequiredIf("Type == ECarType.Truck", ErrorMessage = "Ładowność wymagana dla ciężarówek")]
        [Display(Name = "Ładowność")]
        public decimal? Capacity { get; set; }
        public string UserName { get; set; }
    }
}