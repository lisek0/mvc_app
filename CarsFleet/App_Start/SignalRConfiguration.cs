﻿using Microsoft.Owin;
using Owin;
[assembly: OwinStartup(typeof(CarsFleet.App_Start.Startup))]
namespace CarsFleet.App_Start
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.MapSignalR();
        }
    }
}
