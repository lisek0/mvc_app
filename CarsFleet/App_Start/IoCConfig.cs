﻿using Autofac;
using System.Web.Mvc;
using Autofac.Integration.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Reflection;
using Autofac.Extras.NLog;

namespace CarsFleet.App_Start
{
    public class IoCConfig
    {
        public static void ConfigureIoCContainer()
        {
            var builder = new ContainerBuilder();

            builder.RegisterModule<DAL.IoCModule>();
            builder.RegisterControllers(typeof(IoCConfig).Assembly);
            builder.RegisterModule<SimpleNLogModule>();

            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}