﻿using CarsFleet.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CarsFleet.Controllers
{
    public class UsersController : Controller
    {
        private readonly IUserService _userService;
        public UsersController(IUserService userService)
        {
            _userService = userService;
        }
        public JsonResult GetUsers()
        {
            return Json(_userService.GetAllUsers().Select(u => new SelectListItem { Text = $"{u.FirstName} {u.LastName}", Value = u.Id.ToString() }), JsonRequestBehavior.AllowGet);
        }
    }
}