﻿using CarsFleet.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kendo.Mvc.UI;
using CarsFleet.ViewModels;
using Kendo.Mvc.Extensions;
using CarsFleet.DAL.Enums;
using Autofac.Extras.NLog;

namespace CarsFleet.Controllers
{
    public class CarsController : Controller
    {
        private readonly ICarsService _carsService;
        private readonly ILogger _logger;
        public CarsController(ICarsService carsService, ILogger logger)
        {
            _carsService = carsService;
            _logger = logger;
        }
        // GET: Cars
        public ActionResult Index()
        {
            _logger.Info("Uruchomienie akcji index");
            
            //var cars = _carsService.GetAllCars();
            return View();
        }
        public JsonResult GetCars([DataSourceRequest] DataSourceRequest request)
        {
            var result = _carsService.GetAllCars().Select(c => new CarsViewModel(c));
            return Json(result.ToDataSourceResult(request));
        }
        public ActionResult Details(int id)
        {
            var car = _carsService.GetById(id);
            return View(new CarsViewModel(car));
        }
        public ActionResult Create()
        {
            ViewBag.AvailableTypes = Enum.GetNames(typeof(ECarType)).Select(e => new SelectListItem() { Text = e, Value = e });
            return View();
        }

        [HttpPost]
        public ActionResult Create(CarsViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (_carsService.Create(model.GetModel()))
                    return RedirectToAction("Index");
            }
            ViewBag.AvailableTypes = Enum.GetNames(typeof(ECarType)).Select(e => new SelectListItem() { Text = e, Value = e });
            return View(model);
        }
        public ActionResult Edit(int id)
        {
            var model = _carsService.GetById(id);
            if (model != null)
                return View(new EditCarViewModel(model));
            return RedirectToAction("Index");
        }
        [HttpPost]
        public ActionResult Edit(EditCarViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                if (_carsService.SetCarUser(viewModel.Id, viewModel.UserId))
                    return RedirectToAction("Index");
            }
            return View(viewModel);
        }
    }
}